package Cliente;

import Janelas.Login;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.*;
import javax.swing.JOptionPane;
import BancoDeDados.Controle;
import BancoDeDados.InterfaceCategoria;
import BancoDeDados.InterfaceFuncionarios;
import BancoDeDados.InterfaceProdutos;

public class Cliente {
    
    private static Controle m;
    private static InterfaceCategoria cat ;
    private static InterfaceFuncionarios func ;
    private static InterfaceProdutos prod ;
    
    
    public static void main(String[] args) {
        try {
            //Registry registry = LocateRegistry.getRegistry("localhost");
            //Controle m = (Controle) registry.lookup("ControladorImpl");
            
            //Controle m = (Controle) Naming.lookup("rmi://localhost:1099/Controle");
            
            //System.out.println(m.LerMensagem());
            
            m = (Controle)Naming.lookup("rmi://localhost:1099/Controle");
            
            cat = (InterfaceCategoria) m.createCategoria();
            func = (InterfaceFuncionarios) m.createFuncionarios();
            prod = (InterfaceProdutos) m.createProdutos();
            
            
            Login login = new Login();
            login.setVisible(true);
            
            
        } catch (RemoteException e){
            System.out.println();
            System.out.println("RemoteExçeption: "+e.toString());
        } catch (Exception e){
            System.out.println();
            System.out.println("exçeption: "+e.toString());
        }
        
    }
    
    public Controle getControle(){
        return this.m;
    }
    
    public InterfaceCategoria getInterfaceCategoria(){
        return this.cat;
    }
    
    public InterfaceFuncionarios getInterfaceFuncionarios(){
        return this.func;
    }
    
    public InterfaceProdutos getInterfaceProdutos(){
        return this.prod;
    }
    
}
package Mail;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lucas
 */
import BancoDeDados.InterfaceFuncionarios;
import BancoDeDados.Controle;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
public class JavaMailApp
{
      public void enviaEmail(String titulo, String mensagem){
            Properties props = new Properties();
            /** Parâmetros de conexão com servidor Gmail */
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");
 
            Session session = Session.getDefaultInstance(props,
                        new javax.mail.Authenticator() {
                             protected PasswordAuthentication getPasswordAuthentication() 
                             {
                                   return new PasswordAuthentication("gabrielrmi.lojas@gmail.com", "balasmoke");
                             }
                        });
 
            /** Ativa Debug para sessão */
            session.setDebug(true);
 
            try {
 
                Controle m = new Cliente.Cliente().getControle();
                
                  Message message = new MimeMessage(session);
                  message.setFrom(new InternetAddress("gabrielrmi.lojas@gmail.com")); //Remetente
                  List <InterfaceFuncionarios> fun = m.buscaAllPorNome_Funcionarios("");
                  Address[] toUser = InternetAddress.parse("gabrielrmi.lojas@gmail.com");
                    for (InterfaceFuncionarios a : fun) {
                        if (a.getStatusFuncionarios() == 2) {
                            toUser = InternetAddress.parse(a.getEmailFuncionarios());
                            message.setRecipients(Message.RecipientType.TO, toUser);
                            message.setSubject(titulo);//Assunto
                            message.setText(mensagem);
                            /**Método para enviar a mensagem criada*/
                            Transport.send(message);
                        }
                    }
                  //toUser = InternetAddress //Destinatário(s)
                    //         .parse("lucasbarrosomendonca@gmail.com, gm.silva111297@gmail.com, walteralves27@gmail.com");  
 
                  message.setRecipients(Message.RecipientType.TO, toUser);
                  message.setSubject(titulo);//Assunto
                  message.setText(mensagem);
                  /**Método para enviar a mensagem criada*/
                  Transport.send(message);
 
                  System.out.println("Feito!!!");
 
             } catch (MessagingException e) {
                  throw new RuntimeException(e);
            } catch (RemoteException ex) {
              Logger.getLogger(JavaMailApp.class.getName()).log(Level.SEVERE, null, ex);
          }
      }
}

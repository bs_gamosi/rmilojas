package BancoDeDados;

import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FuncionariosDAO {
    public int inserir_Funcionarios(InterfaceFuncionarios a) throws RemoteException {
        String query = "INSERT INTO funcionarios (nomeFuncionarios, usuarioFuncionarios"
                + ",senhaFuncionarios, emailFuncionarios, statusFuncionarios) VALUES (?,?,?,?,?)";
        return MySql.executeQuery(query, a.getNomeFuncionarios()
                ,a.getUsuarioFuncionarios()
                ,a.getSenhaFuncionarios()
                ,a.getEmailFuncionarios()
                ,a.getStatusFuncionarios());
    }

    public void atualizar_Funcionarios(InterfaceFuncionarios a) throws RemoteException {
        String query = "UPDATE funcionarios SET nomeFuncionarios = ?, usuarioFuncionarios = ?,"
                + "senhaFuncionarios = ?, emailFuncionarios = ?,statusFuncionarios = ?"
                + " WHERE idFuncionarios = ?";
        MySql.executeQuery(query, a.getNomeFuncionarios(),a.getUsuarioFuncionarios()
                , a.getSenhaFuncionarios(), a.getEmailFuncionarios(),a.getStatusFuncionarios()
                , a.getIdFuncionarios());
    }
    
    public void excluir_Funcionarios(InterfaceFuncionarios a) throws RemoteException {
        String query = "DELETE FROM funcionarios WHERE idFuncionarios = ?";
        MySql.executeQuery(query, a.getIdFuncionarios());
    }
    
    public InterfaceFuncionarios busca_Funcionarios(String name)throws RemoteException{
        return (InterfaceFuncionarios) buscador_Funcionarios("SELECT * FROM funcionarios WHERE nomeFuncionarios LIKE '%"+name+"%'" ).get(0);
    }
    
    public InterfaceFuncionarios buscaLogin_Funcionarios(String user,String senha)throws RemoteException{
        return (InterfaceFuncionarios) buscador_Funcionarios("SELECT * FROM funcionarios WHERE usuarioFuncionarios LIKE '%"+user+"%' and senhaFuncionarios LIKE '%"+senha+"%'" ).get(0);
    }    
    
    public InterfaceFuncionarios busca_Funcionarios(int id) throws RemoteException{
        return (InterfaceFuncionarios) buscador_Funcionarios("SELECT * FROM funcionarios WHERE idFuncionarios = "+id).get(0);
    }
      
    public ArrayList<InterfaceFuncionarios> buscaAllPorNome_Funcionarios(String name)throws RemoteException{
        return buscador_Funcionarios("SELECT * FROM funcionarios WHERE nomeFuncionarios LIKE '%"+name+"%'" );
    }
    
    public ArrayList<InterfaceFuncionarios> buscaAllPorId_Funcionarios(int id)throws RemoteException{
        return buscador_Funcionarios("SELECT * FROM funcionarios WHERE idFuncionarios = "+id);
    }
    
    
    
    
    private ArrayList buscador_Funcionarios(String query)throws RemoteException{
        ArrayList <InterfaceFuncionarios> lista = new ArrayList<>();
        ResultSet rs = null;
        rs = MySql.getResultSet(query);
        try {
            while (rs.next()) {
                InterfaceFuncionarios a = new Funcionarios (rs.getInt("idFuncionarios")
                        ,rs.getString("nomeFuncionarios")
                        ,rs.getString("usuarioFuncionarios")
                        ,rs.getString("senhaFuncionarios")
                        ,rs.getString("emailFuncionarios")
                        ,rs.getInt("statusFuncionarios"));           
                lista.add(a);
            }
            rs.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return lista;
    }
}
package BancoDeDados;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


//ProdutosDAOInterface
public class ProdutosDAO {
    
    public int inserir_Produtos(InterfaceProdutos a) throws RemoteException {
        String query = "INSERT INTO produtos (nomeProdutos,qtdProdutos,precoProdutos,idProdutos_Categoria)"
                + " VALUES (?,?,?,?)";
        return MySql.executeQuery(query, a.getNomeProdutos()
                ,a.getQtdProdutos()
                ,a.getPrecoProdutos()
                ,a.getIdProdutos_Categoria());
    }

    public void atualizar_Produtos(InterfaceProdutos a) throws RemoteException {
        String query = "UPDATE produtos SET nomeProdutos = ?, qtdProdutos = ?,"
                + "precoProdutos = ?, idProdutos_Categoria = ?"
                + " WHERE idProdutos = ?";
        MySql.executeQuery(query, a.getNomeProdutos(), a.getQtdProdutos()
                ,a.getPrecoProdutos(), a.getIdProdutos_Categoria());
    }
    //UPDATE produtos SET qtdProdutos = qtdProdutos-? ,WHERE idProdutos = ?
    //UPDATE produtos SET qtdProdutos = qtdProdutos-5 WHERE idProdutos = 1
    public void venda_Produtos(int id, int qtd) throws RemoteException {
        String query = "UPDATE produtos SET qtdProdutos = qtdProdutos-"+qtd+" WHERE idProdutos = "+id;
        MySql.executeQuery(query);
    }
    
    public void excluir_Produtos(InterfaceProdutos a) throws RemoteException {
        String query = "DELETE FROM produtos WHERE idProdutos = ?";
        MySql.executeQuery(query, a.getIdProdutos());
    }
    
    public InterfaceProdutos busca_Produtos(String name)throws RemoteException{
        return (InterfaceProdutos) buscador_Produtos("SELECT * FROM produtos WHERE nomeProdutos LIKE '%"+name+"%'" ).get(0);
    }
    
    
    public InterfaceProdutos busca_Produtos(int id) throws RemoteException{
        return (InterfaceProdutos) buscador_Produtos("SELECT * FROM produtos WHERE idProdutos = "+id).get(0);
    }
    
    public ArrayList<InterfaceProdutos> buscaAllPorNome_Produtos(String name)throws RemoteException{
        return buscador_Produtos("SELECT * FROM produtos WHERE nomeProdutos LIKE '%"+name+"%'" );
    }
    
    public ArrayList<InterfaceProdutos> buscaAllPorId_Produtos(int id)throws RemoteException{
        return buscador_Produtos("SELECT * FROM produtos WHERE idProdutos = "+id);
    }
    
    private ArrayList buscador_Produtos(String query)throws RemoteException{
        ArrayList <InterfaceProdutos> lista = new ArrayList<>();
        ResultSet rs = null;
        rs = MySql.getResultSet(query);
        try {
            while (rs.next()) {
                InterfaceProdutos a = new Produtos (rs.getInt("idProdutos")
                        ,rs.getString("nomeProdutos")
                        ,rs.getInt("qtdProdutos")
                        ,rs.getDouble("precoProdutos")
                        ,rs.getInt("idProdutos_Categoria"));           
                lista.add(a);
            }
            rs.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return lista;
    }
}
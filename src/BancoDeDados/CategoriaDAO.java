package BancoDeDados;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CategoriaDAO {
    
    public int inserir_Categoria(InterfaceCategoria a) throws RemoteException{
        String query = "INSERT INTO Categoria (nomeCategoria) VALUES (?)";
        return MySql.executeQuery(query, a.getNomeCategoria());
    }

    public void atualizar_Categoria(InterfaceCategoria a) throws RemoteException{
        String query = "UPDATE Categoria SET nomeCategoria = ? WHERE idCategoria = ?";
        MySql.executeQuery(query, a.getNomeCategoria(), a.getIdCategoria());
    }
    
    public void excluir_Categoria(InterfaceCategoria a) throws RemoteException {
        String query = "DELETE FROM Categoria WHERE idCategoria = ?";
        MySql.executeQuery(query, a.getIdCategoria());
    }
    
    public InterfaceCategoria busca_Categoria(String name)throws RemoteException{
        return (InterfaceCategoria) buscador_Categoria("SELECT * FROM categoria WHERE nomeCategoria LIKE '%"+name+"%'" ).get(0);
    }
    
    
    public InterfaceCategoria busca_Categoria(int id) throws RemoteException{
        return (InterfaceCategoria) buscador_Categoria("SELECT * FROM categoria WHERE idCategoria = "+id).get(0);
    }
    
    public ArrayList<InterfaceCategoria> busca_Categoria() throws RemoteException{
        return buscador_Categoria("SELECT * FROM categoria");
    }
      
    public ArrayList buscaAllPorNome_Categoria(String name)throws RemoteException{
        return buscador_Categoria("SELECT * FROM categoria WHERE nomeCategoria LIKE '%"+name+"%'" );
    }
    
    public ArrayList buscaAllPorId_Categoria(int id)throws RemoteException{
        return buscador_Categoria("SELECT * FROM categoria WHERE idCategoria = "+id);
    }
    
    private ArrayList buscador_Categoria(String query) throws RemoteException{
        ArrayList <InterfaceCategoria> lista = new ArrayList<>();
        ResultSet rs = null;
        rs = MySql.getResultSet(query);
        try {
            while (rs.next()) {
                InterfaceCategoria a = new Categoria (rs.getInt("idCategoria"),rs.getString("nomeCategoria"));           
                lista.add(a);
            }
            rs.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return lista;
    }

}
package BancoDeDados;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Categoria extends UnicastRemoteObject implements InterfaceCategoria{

    public Categoria() throws RemoteException {
        super();
    }
    
    private int idCategoria;
    private String nomeCategoria;

    public Categoria(int idCategoria, String nomeCategoria) throws RemoteException{
        this.idCategoria = idCategoria;
        this.nomeCategoria = nomeCategoria;
    }

    public Categoria(int idCategoria) throws RemoteException{
        this.idCategoria = idCategoria;
    }

    public Categoria(String nomeCategoria) throws RemoteException{
        this.nomeCategoria = nomeCategoria;
    }

    public int getIdCategoria() throws RemoteException{
        return idCategoria;
    }

    public String getNomeCategoria() throws RemoteException{
        return nomeCategoria;
    }

    public void setIdCategoria(int idCategoria) throws RemoteException{
        this.idCategoria = idCategoria;
    }

    public void setNomeCategoria(String nomeCategoria) throws RemoteException{
        this.nomeCategoria = nomeCategoria;
    }
    
    
    
}

package BancoDeDados;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class Funcionarios extends UnicastRemoteObject implements InterfaceFuncionarios{
    
    private int idFuncionarios;
    private String nomeFuncionarios;
    private String usuarioFuncionarios;
    private String senhaFuncionarios;
    private String emailFuncionarios;
    private int statusFuncionarios;

    public Funcionarios() throws RemoteException {
        super();
    }
    
    public Funcionarios(int idFuncionarios, String nomeFuncionarios, String usuarioFuncionarios, String senhaFuncionarios, String emailFuncionarios, int statusFuncionarios) throws RemoteException{
        this.idFuncionarios = idFuncionarios;
        this.nomeFuncionarios = nomeFuncionarios;
        this.usuarioFuncionarios = usuarioFuncionarios;
        this.senhaFuncionarios = senhaFuncionarios;
        this.emailFuncionarios = emailFuncionarios;
        this.statusFuncionarios = statusFuncionarios;
    }

    public Funcionarios(String nomeFuncionarios, String usuarioFuncionarios, String senhaFuncionarios, String emailFuncionarios, int statusFuncionarios) throws RemoteException{
        this.nomeFuncionarios = nomeFuncionarios;
        this.usuarioFuncionarios = usuarioFuncionarios;
        this.senhaFuncionarios = senhaFuncionarios;
        this.emailFuncionarios = emailFuncionarios;
        this.statusFuncionarios = statusFuncionarios;
    }

    public Funcionarios(int idFuncionarios) throws RemoteException{
        this.idFuncionarios = idFuncionarios;
    }

    public int getIdFuncionarios() throws RemoteException{
        return idFuncionarios;
    }

    public String getNomeFuncionarios() throws RemoteException{
        return nomeFuncionarios;
    }

    public String getUsuarioFuncionarios() throws RemoteException{
        return usuarioFuncionarios;
    }

    public String getSenhaFuncionarios() throws RemoteException{
        return senhaFuncionarios;
    }

    public String getEmailFuncionarios() throws RemoteException{
        return emailFuncionarios;
    }

    public int getStatusFuncionarios() throws RemoteException{
        return statusFuncionarios;
    }

    public void setIdFuncionarios(int idFuncionarios) throws RemoteException{
        this.idFuncionarios = idFuncionarios;
    }

    public void setNomeFuncionarios(String nomeFuncionarios) throws RemoteException{
        this.nomeFuncionarios = nomeFuncionarios;
    }

    public void setUsuarioFuncionarios(String usuarioFuncionarios) throws RemoteException{
        this.usuarioFuncionarios = usuarioFuncionarios;
    }

    public void setSenhaFuncionarios(String senhaFuncionarios) throws RemoteException{
        this.senhaFuncionarios = senhaFuncionarios;
    }

    public void setEmailFuncionarios(String emailFuncionarios) throws RemoteException{
        this.emailFuncionarios = emailFuncionarios;
    }

    public void setStatusFuncionarios(int statusFuncionarios) throws RemoteException{
        this.statusFuncionarios = statusFuncionarios;
    }
    
    
    
}
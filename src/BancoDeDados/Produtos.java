package BancoDeDados;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class Produtos extends UnicastRemoteObject implements InterfaceProdutos{
    
    private int idProdutos;
    private String nomeProdutos;
    private int qtdProdutos;
    private Double precoProdutos;
    private int idProdutos_Categoria;

    public Produtos() throws RemoteException {
        super();
    }
    
    public Produtos(int idProdutos, String nomeProdutos, int qtdProdutos, Double precoProdutos, int idProdutos_Categoria) throws RemoteException{
        this.idProdutos = idProdutos;
        this.nomeProdutos = nomeProdutos;
        this.qtdProdutos = qtdProdutos;
        this.precoProdutos = precoProdutos;
        this.idProdutos_Categoria = idProdutos_Categoria;
    }

    public Produtos(String nomeProdutos, int qtdProdutos, Double precoProdutos, int idProdutos_Categoria) throws RemoteException{
        this.nomeProdutos = nomeProdutos;
        this.qtdProdutos = qtdProdutos;
        this.precoProdutos = precoProdutos;
        this.idProdutos_Categoria = idProdutos_Categoria;
    }

    public Produtos(int idProdutos) throws RemoteException{
        this.idProdutos = idProdutos;
    }

    public int getIdProdutos() throws RemoteException{
        return idProdutos;
    }

    public String getNomeProdutos() throws RemoteException{
        return nomeProdutos;
    }

    public int getQtdProdutos() throws RemoteException{
        return qtdProdutos;
    }

    public Double getPrecoProdutos() throws RemoteException{
        return precoProdutos;
    }

    public int getIdProdutos_Categoria() throws RemoteException{
        return idProdutos_Categoria;
    }

    public void setIdProdutos(int idProdutos) throws RemoteException{
        this.idProdutos = idProdutos;
    }

    public void setNomeProdutos(String nomeProdutos) throws RemoteException{
        this.nomeProdutos = nomeProdutos;
    }

    public void setQtdProdutos(int qtdProdutos) throws RemoteException{
        this.qtdProdutos = qtdProdutos;
    }

    public void setPrecoProdutos(Double precoProdutos) throws RemoteException{
        this.precoProdutos = precoProdutos;
    }

    public void setIdProdutos_Categoria(int idProdutos_Categoria) throws RemoteException{
        this.idProdutos_Categoria = idProdutos_Categoria;
    }
    
    
    
    
}
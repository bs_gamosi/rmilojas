package Servidor;

import BancoDeDados.Controle;
import BancoDeDados.Categoria;
import BancoDeDados.CategoriaDAO;
import BancoDeDados.Funcionarios;
import BancoDeDados.FuncionariosDAO;
import BancoDeDados.InterfaceCategoria;
import BancoDeDados.InterfaceFuncionarios;
import BancoDeDados.InterfaceProdutos;
import BancoDeDados.Produtos;
import BancoDeDados.ProdutosDAO;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ControladorImpl extends UnicastRemoteObject implements Controle {

    private final CategoriaDAO CategoriaDAO = new CategoriaDAO();
    private final FuncionariosDAO FuncionariosDAO = new FuncionariosDAO();
    private final ProdutosDAO ProdutosDAO = new ProdutosDAO();

    
    
    public void inserir_Categoria(String name) throws RemoteException {
        InterfaceCategoria categoria = new Categoria(name);
        CategoriaDAO.inserir_Categoria(categoria);
    }

    public void atualizar_Categoria(String name, int id) throws RemoteException {
        InterfaceCategoria categoria = new Categoria(id, name);
        CategoriaDAO.atualizar_Categoria(categoria);
    }

    public void excluir_Categoria(int id) throws RemoteException {
        InterfaceCategoria categoria = new Categoria(id);
        CategoriaDAO.excluir_Categoria(categoria);
    }

    public InterfaceCategoria busca_Categoria(String name) throws RemoteException {
        return CategoriaDAO.busca_Categoria(name);
    }

    public InterfaceCategoria busca_Categoria(int id) throws RemoteException {
        return CategoriaDAO.busca_Categoria(id);
    }
    
    public ArrayList<InterfaceCategoria> busca_Categoria() throws RemoteException {
        return CategoriaDAO.busca_Categoria();
    }
    
    public ArrayList<InterfaceCategoria> buscaAllPorNome_Categoria(String name) throws RemoteException {
        return CategoriaDAO.buscaAllPorNome_Categoria(name);
    }

    public ArrayList<InterfaceCategoria> buscaAllPorId_Categoria(int id) throws RemoteException {
        return CategoriaDAO.buscaAllPorId_Categoria(id);
    }

    ////////////////////////////////////////////////////////////////////////////
    public void inserir_Funcionarios(String name, String user, String senha, String email, int status) throws RemoteException {
        InterfaceFuncionarios funcionario = new Funcionarios(name, user, senha, email, status);
        FuncionariosDAO.inserir_Funcionarios(funcionario);
    }

    public void atualizar_Funcionarios(String name, String user, String senha, String email, int status, int id) throws RemoteException {
        InterfaceFuncionarios funcionario = new Funcionarios(id, name, user, senha, email, status);
        FuncionariosDAO.atualizar_Funcionarios(funcionario);
    }

    public void excluir_Funcionarios(int id) throws RemoteException {
        InterfaceFuncionarios funcionario = new Funcionarios(id);
        FuncionariosDAO.excluir_Funcionarios(funcionario);
    }

    public InterfaceFuncionarios busca_Funcionarios(String name) throws RemoteException {
        return FuncionariosDAO.busca_Funcionarios(name);
    }

    public InterfaceFuncionarios busca_Funcionarios(int id) throws RemoteException {
        return FuncionariosDAO.busca_Funcionarios(id);
    }

    public ArrayList<InterfaceFuncionarios> buscaAllPorNome_Funcionarios(String name) throws RemoteException {
        return FuncionariosDAO.buscaAllPorNome_Funcionarios(name);
    }

    public ArrayList<InterfaceFuncionarios> buscaAllPorId_Funcionarios(int id) throws RemoteException {
        return FuncionariosDAO.buscaAllPorId_Funcionarios(id);
    }
    
    public int buscaLogin_Funcionarios(String user, String senha)throws RemoteException{
        return FuncionariosDAO.buscaLogin_Funcionarios(user, senha).getIdFuncionarios();
    }

    ////////////////////////////////////////////////////////////////////////////
    public void inserir_Produtos(String name, int qtd, Double preco, int idCatego) throws RemoteException {
        InterfaceProdutos produto = new Produtos(name, qtd, preco, idCatego);
        ProdutosDAO.inserir_Produtos(produto);
    }

    public void atualizar_Produtos(String name, int qtd, Double preco, int idCatego, int id) throws RemoteException {
        InterfaceProdutos produto = new Produtos(id, name, qtd, preco, idCatego);
        ProdutosDAO.atualizar_Produtos(produto);
    }
    
    public void venda_Produtos(int id, int qtd) throws RemoteException {
        ProdutosDAO.venda_Produtos(id, qtd);
    }

    public void excluir_Produtos(int id) throws RemoteException {
        InterfaceProdutos produto = new Produtos(id);
        ProdutosDAO.excluir_Produtos(produto);
    }

    public InterfaceProdutos busca_Produtos(String name) throws RemoteException {
        return ProdutosDAO.busca_Produtos(name);
    }

    public InterfaceProdutos busca_Produtos(int id) throws RemoteException {
        return ProdutosDAO.busca_Produtos(id);
    }

    public ArrayList<InterfaceProdutos> buscaAllPorNome_Produtos(String name) throws RemoteException {
        return ProdutosDAO.buscaAllPorNome_Produtos(name);
    }

    public ArrayList<InterfaceProdutos> buscaAllPorId_Produtos(int id) throws RemoteException {
        return ProdutosDAO.buscaAllPorId_Produtos(id);
    }

    ////////////////////////////////////////////////////////////////////////////
    public InterfaceCategoria createCategoria() throws RemoteException {
        Categoria categoria = new Categoria();
        
        return categoria;
    }

    public InterfaceFuncionarios createFuncionarios() throws RemoteException{
        Funcionarios func = new Funcionarios();
        
        return func;
    }

    public InterfaceProdutos createProdutos() throws RemoteException{
        Produtos prod = new Produtos();
        
        return prod;        
    }

    ////////////////////////////////////////////////////////////////////////////
    private static String SERVIDOR = "localhost";
    private static Integer PORTA = 1099;
    private static String SERVICO = "mensageiro";

    public static String geturi() {
        String uri = String.format("rmi://%s:%d/%s", SERVIDOR, PORTA, SERVICO);
        return uri;
    }

    //rmi://localhost:1099/Controle
    public ControladorImpl() throws RemoteException {
        super();
    }

    public void jhonso() throws RemoteException {
        System.err.println("funciona");
    }

}
